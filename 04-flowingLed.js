/* flowingLed */
console.log('flowingLed.js');
/* requirements */
const Gpio = require('onoff').Gpio;

/* variables */
let directionOfFlow = 'up';
let indexCount = 0;

/* pins */
const allLeds = [
	new Gpio(4, 'out'),
	new Gpio(17, 'out'),
	new Gpio(27, 'out'),
	new Gpio(22, 'out'),
	new Gpio(5, 'out'),
	new Gpio(6, 'out'),
	new Gpio(13, 'out'),
	new Gpio(19, 'out'),
	new Gpio(26, 'out'),
	/* row 2 */
	new Gpio(18, 'out'),
	new Gpio(23, 'out'),
	new Gpio(24, 'out'),
	new Gpio(25, 'out'),
	new Gpio(12, 'out'),
	new Gpio(16, 'out'),
	new Gpio(20, 'out'),
	new Gpio(21, 'out')
];

/* function to change value of pins */
const useLed = (led, value) => led.writeSync(value);
/* function to flow all leds */
const flowingLed = () => {
	allLeds.forEach((led, index, leds) => {
		useLed(led, 0);
	});

	if (indexCount == 0) directionOfFlow = 'up';
	if (indexCount >= allLeds.length) directionOfFlow = 'down';
	if (directionOfFlow == 'down') indexCount--;
	useLed(allLeds[indexCount], 1);
	console.log(`indexCount: ${indexCount}  GPIO: ${allLeds[indexCount]._gpio}  Value: ${allLeds[indexCount].readSync()}`);

	if (directionOfFlow == 'up') indexCount++;
};
/* function to clean everything up */
const unexportOnClose = () => {
	clearInterval(flowInterval);
	allLeds.forEach((val) => {
		useLed(val, 0);
		val.unexport();
	});
	console.log('end of program');
}
/* function and trigger to flow all leds */
const flowInterval = setInterval(flowingLed, 100);

/* by pressing CTRL + C it cleans everything up */
process.on('SIGINT', unexportOnClose);