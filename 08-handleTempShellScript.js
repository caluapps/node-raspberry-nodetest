console.log('handleTempShellScript.js');

const exec = require('child_process').exec;
const pathToScript = '/home/pi/Desktop/projects/nodetest/scripts/handle_TempShellScript';
const pathToShutdownScript = '/home/pi/Desktop/projects/nodetest/scripts/handle_TempShutdownShellScript';

/* interact with the output of a shell script */
exec(pathToScript, function(error, stdout, stderr) {
	if (error != null) console.log('exec error: ', error);
	if (stderr) console.log('stderr: ', stderr);

	console.log('stdout: ', stdout);
	
	// console.log(stdout.replace(/[temp='c]/gi, ''));
	let currentTemp = stdout.replace(/[temp='c]/gi, '');
	if (currentTemp < 40) {
		console.log('Everything is fine: ', currentTemp);
	}

	if (currentTemp >= 40 && currentTemp <= 47) {
		console.log('Fans should start blowing now: ', currentTemp)
	}

	if (currentTemp >= 48) {
		console.log('Critical: ', currentTemp);

		console.log('Raspberry is now shutting down!');

		let countdown = 5;
		let countdownInterval = setInterval(function() {
			console.log(countdown);
			countdown--;

			if (countdown === 0) {
				clearInterval(countdownInterval);
				// console.log('aha');
				exec(pathToShutdownScript, function(error, stdout, stderr) {
						if (error != null) console.log('exec error: ', error);
						if (stderr) console.log('stderr: ', stderr);

						console.log('stdout: ', stdout);
				});
			}
		}, 1000);
	}
});