console.log('slave.js');

/* dependencies */
	let io = require('socket.io-client');

	let socket = io.connect('http://105.78.234.73:3000/', {
		reconnection: true
	});


/* send something */
	socket.emit('sentFromClient', 'hello from client');


/* connect event */
	socket.on('connection', function() {
		console.log('zero connected to http://105.78.234.73:3000/');
	});

/* receive something */
	socket.on('sentFromServer', function(data) {
		console.log('received data from server: ', data);
	});

/* disconnect event */
	socket.on('disconnect', function() {
		console.log('zero disconnected from server');
	});