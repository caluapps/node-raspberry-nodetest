console.log('master.js');

/* dependencies */
	const path = require('path');
	const http = require('http');
	const express = require('express');
	const socketIO = require('socket.io');

/* clear path */
	const publicPath = path.join(__dirname, 'public');

/* Port */
	const port = process.env.PORT || 3000;

/* app, server, io */
	let app = express();
	let server = http.createServer(app);
	let io = socketIO(server);

/* serve public folder */
	app.use(express.static(publicPath));

/* events */
/* connect event */
	io.on('connection', (socket) => {
		console.log('device connected');


	/* send something */
		socket.emit('sentFromServer', 'hello from server');


	/* receive something */
		socket.on('sentFromClient', (data) => {
			console.log('received data from client', data);
		});

	/* disconnect event */
		socket.on('disconnect', () => {
			console.log('device disconnected');
		});
	});

/* by pressing CTRL + C it cleans everything up */
	process.on('SIGINT', function () {
		process.exit();
	});

/* server listening on port */
	server.listen(port, () => {
		console.log(`-- App: Started up at port ${port}`);
	});