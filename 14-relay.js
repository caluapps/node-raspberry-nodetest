/* relay */
	console.log('relay.js');

/* requirements */
	const Gpio = require('onoff').Gpio;

/* pins */
	const relais = new Gpio(17, 'out');
	const pushButton = new Gpio(27, 'in', 'both');

/* function to change value of pins */
	const useLed = (led, value) => led.writeSync(value);

/* function to clean everything up */
	const unexportOnClose = () => {
		relais.writeSync(0);
		relais.unexport();
		pushButton.unwatchAll();
		pushButton.unexport();
		console.log('end of program');
	}

/* listener on pin w/ button */
	pushButton.watch((err, value) => {
		console.log('value: ', value);
		if (err) {
			console.error('There was an error', err);
			throw err;
		}
		console.log('test');
		useLed(relais, !value);
	});

/* by pressing CTRL + C it cleans everything up */
	process.on('SIGINT', unexportOnClose);