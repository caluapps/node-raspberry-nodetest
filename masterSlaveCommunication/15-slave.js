console.log('slave.js');
/*
	var io = require('socket.io-client'),
	// socket = io.connect('localhost', {
	socket = io.connect('105.78.234.73', {
		// port: 1337
		port: 3000
	}); */

	import io from 'socket.io-client';

	const socket = io('http://localhost');

/* connect event */
	socket.on('connect', function () {
		console.log('slave connected');
	});

/* disconnect event */
	socket.on('disconnect', function() {
		console.log('slave disconnected from master');

	});

	// socket.emit('news', { hello: 'world' });