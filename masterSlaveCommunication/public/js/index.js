console.log('index.js');

/* server location */
  let socket = io('http://105.78.234.73:3000');

/* connect event */
  socket.on('connect', function() {
    console.log('connected to server');

  });

/* disconnect event */
  socket.on('disconnect', function() {
    console.log('disconnected from server');

  });


/* client listener if pump web button has been pressed */
  socket.on('pumpWebSwitch', function(data) {
    console.log('pumpWebSwitch ', data);
  });



$(document).ready(function() {

});