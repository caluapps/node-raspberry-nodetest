console.log('master.js');

/* dependencies */
	const path = require('path');
	const http = require('http');
	const express = require('express');
	const socketIO = require('socket.io');

	const Gpio = require('onoff').Gpio;

/* pins */
	// let ledConnected = new Gpio(17, 'out');

/* clear path */
	const publicPath = path.join(__dirname, 'public');

/* Port */
	const port = process.env.PORT || 3000;

/* app, server, io */
	let app = express();
	let server = http.createServer(app);
	let io = socketIO(server);

/* serve public folder */
	app.use(express.static(publicPath));

/* functions */
/* function to change value of pins */
	const useLed = (led, value) => led.writeSync(value);

/* function to clean everything up
	const unexportOnClose = () => {
		led.unexport();
		console.log('led now off');
		console.log('end of program');
	}; */


/* events */
/* connect event */
	io.on('connection', (socket) => {
		console.log('device connected');

		/* disconnect event */
		socket.on('disconnect', () => {
			console.log('device disconnected');
			// useLed(ledConnected, 0);
		});
	});



/* by pressing CTRL + C it cleans everything up */
	process.on('SIGINT', function () {
		// ledConnected.writeSync(0);
		// ledConnected.unexport();
		process.exit();
	});

/* server listening on port */
	server.listen(port, () => {
	  console.log(`-- App: Started up at port ${port}`);
	});