console.log('index.js');

/* server location */
let socket = io('raspberrypi3:3000');

$(document).ready(function() {

/* connect event */
	socket.on('connect', function() {
		console.log('connected to server');
	});

/* receive data event */
	socket.on('sendStat', function(data) {
		// console.log('receiving data from server');

	});

/* receive response event */
	socket.on('sendResponsMessage', function(message) {
		console.log('receiving message from server');

	});

/* receive countdown response event */
	socket.on('countdownMessage', function(message) {
		console.log('receiving countdown message from server');

	});

/* disconnect event */
	socket.on('disconnect', function() {
		console.log('disconnected from server');
	});

});