console.log('piCamera');

/* dependencies */
	const path = require('path');
	const http = require('http');
	const express = require('express');
	const socketIO = require('socket.io');
	const exec = require('child_process').exec;
	const fs = require('fs');

/* clear path */
	const publicPath = path.join(__dirname, '../public');

/* Port */
	const port = process.env.PORT || 3000;

/* app, server, io */
	let app = express();
	let server = http.createServer(app);
	let io = socketIO(server);

/* serve public folder */
	app.use(express.static(publicPath));



/* functions */

	// function to clean everything up
	const unexportOnClose = () => {
	/*
		gpioPins.forEach(function(currentValue) {
			currentValue.writeSync(0);
			currentValue.unexport();
		});
		console.log('\nswitched all led to off and unexport!');
		console.log('\nbye')
	*/
	};


/* events */

	// connect event
	io.on('connection', (socket) => {
		console.log('Log: device connected');
		getInfos();

		// Listener for pushedButton
		socket.on('pushedButton', (button) => {
			console.log('Log: pushedButton', button);

		});

		// Listener for switchPin
		socket.on('switchPin', (switchPinData) => {
			console.log('Log: switchPin', switchPinData);

		});

		// disconnect event
		socket.on('disconnect', () => {
			console.log('device disconnected');
		});
	});


/* by pressing CTRL + C it cleans everything up */
	process.on('SIGINT', function () {
		unexportOnClose();
		process.exit();
	});

/* server listening on port */
	server.listen(port, () => {
		console.log(`-- App: Started up at port ${port}`);
	});