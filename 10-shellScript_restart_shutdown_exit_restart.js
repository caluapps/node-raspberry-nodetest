console.log('shellScript_restart_shutdown_restart.js');
/* dependencies */
	const exec = require('child_process').exec;

/* scripts */
	const pathToRestartRBPScript = '/home/pi/Desktop/projects/nodetest/scripts/script_restartRaspberry';
	const pathToShutdownRBPScript = '/home/pi/Desktop/projects/nodetest/scripts/script_shutdownRaspberry';
	const pathToStartAuxiliaryNodeAppScript = '/home/pi/Desktop/projects/nodetest/scripts/script_startAuxiliaryNodeApp';

/* exec */
/* restart pi */
	exec(pathToRestartRBPScript, function(error, stdout, stderr) {
		if (error != null) console.log('exec error: ', error);
		if (stderr) console.log('stderr: ', stderr);

		console.log('stdout:', stdout);
	});

/* shutdown pi */
	exec(pathToShutdownRBPScript, function(error, stdout, stderr) {
		if (error != null) console.log('exec error: ', error);
		if (stderr) console.log('stderr: ', stderr);

		console.log('stdout:', stdout);
	});

/* exit node app */
	let countdown = 5;
	let countdownInterval = setInterval(function() {
		console.log(countdown);
		countdown--;

		if (countdown === 0) {
			clearInterval(countdownInterval);
			console.log('bye');
			process.exit();
		}
	}, 1000);

/*	does not work yet
 starts auxiliary node app to restart main node app
	exec(pathToStartAuxiliaryNodeAppScript, function(error, stdout, stderr) {
		if (error != null) console.log('exec error: ', error);
		if (stderr) console.log('stderr: ', stderr);

		console.log('stdout:', stdout);
	}); */
