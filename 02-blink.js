/* blink */
console.log('blink.js');
/* requirements */
const Gpio = require('onoff').Gpio;

/* pins */
const ledPin = new Gpio(17, 'out');

/* function to change value of pins */
const useLed = (led, value) => led.writeSync(value);
/* function to toggle in blinking */
const blinkLed = () => {
	if (ledPin.readSync()) {
		useLed(ledPin, 0);
		console.log('off');
	} else {
		useLed(ledPin, 1);
		console.log('on');
	}
};
/* function to set the intervall of blinking*/
const blinkInterval = setInterval(blinkLed, 250);
/* function to clean everything up */
const endBlinking = () => {
	clearInterval(blinkInterval);
	ledPin.writeSync(0);
	ledPin.unexport();
	console.log('led now off');
	console.log('end of program');
};

/* set the time when to clean up */
setTimeout(endBlinking, 5000);

/* by pressing CTRL + C it cleans everything up */
process.on('SIGINT', endBlinking);
