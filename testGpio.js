/* test gpio */
console.log('testGpio.js');
/* requirements */
const Gpio = require('onoff').Gpio;

/* pins */
const allLeds = [
	/* row 1 */
	new Gpio(4, 'out'),
	new Gpio(17, 'out'),
	new Gpio(27, 'out'),
	new Gpio(22, 'out'),
	new Gpio(5, 'out'),
	new Gpio(6, 'out'),
	new Gpio(13, 'out'),
	new Gpio(19, 'out'),
	new Gpio(26, 'out'),
	/* row 2 */
	new Gpio(18, 'out'),
	new Gpio(23, 'out'),
	new Gpio(24, 'out'),
	new Gpio(25, 'out'),
	new Gpio(12, 'out'),
	new Gpio(16, 'out'),
	new Gpio(20, 'out'),
	new Gpio(21, 'out')
];

/* function to change value of pins */
const useLed = (led, value) => led.writeSync(value);
/* function to flow all leds */
const showAllLed = () => {
	allLeds.forEach((led, index, leds) => {
		useLed(led, 1);
		console.log(`GPIO: ${led._gpio} Value: ${led.readSync()}`);
		/* show all properties of object led
		for(var key in led) {
			console.log(key);
		} */
	});
};
/* function to clean everything up */
const unexportOnClose = () => {
	// clearInterval(flowInterval);
	allLeds.forEach((val) => {
		useLed(val, 0);
		val.unexport();
	});
	console.log('end of program');
}
/* function and trigger to end program after 5 seconds */
const endProgramm = setTimeout(unexportOnClose, 5000);

/* trigger for main function to show up all leds */
showAllLed();

/* trigger to clean up everything by pressing CTRL + C */
process.on('SIGINT', unexportOnClose);