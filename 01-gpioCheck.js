/* Check accessibility */
console.log('gpioCheck.js');
/* requirements */
const Gpio = require('onoff').Gpio;

/* pins */
let led = new Gpio(17, 'out');

/* function to change value of pins */
const useLed = (led, value) => led.writeSync(value);
/* function to clean everything up */
const unexportOnClose = () => {
	led.unexport();
	console.log('led now off');
	console.log('end of program');
};

/* checking accessibility */
if (Gpio.accessible) {
	console.log('gpio accessable');

	/* toggles led to on if off */
	if (!led.readSync()) {
		useLed(led, 1);
		console.log('led now on');
	}

} else {
	led = {
		writeSync: (value) => {
			console.log('virtual led now uses value: ' + value);
    		}
	};
}

// console.log('led.readSync:', led.readSync());

// useLed(led, 1);
// console.log('led.readSync:', led.readSync());

/* set the time to 5 seconds to clean up */
setTimeout(unexportOnClose, 5000);

/* by pressing CTRL + C it cleans everything up */
process.on('SIGINT', unexportOnClose);
