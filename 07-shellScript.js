console.log('shellScript.js');

const exec = require('child_process').exec;
const pathToFileHelloWorld = '/home/pi/Desktop/projects/nodetest/scripts/first_script';
const pathToFileRaspberryTemp = '/home/pi/Desktop/projects/nodetest/scripts/raspberry_temp';


/* run hello world script */
exec(pathToFileHelloWorld, function(error, stdout, stderr) {
	if (error != null) {
		console.log('exec error: ', error);
	}
	if (stderr) {
		console.log('stderr: ', stderr);
	}

	console.log('stdout: ', stdout);
});

// run temp of rpis cpu
exec(pathToFileRaspberryTemp, function(error, stdout, stderr) {
	if (error != null) {
		console.log('exec error: ', error);
	}
	if (stderr) {
		console.log('stderr: ', stderr);
	}

	console.log('stdout: ', stdout);
});