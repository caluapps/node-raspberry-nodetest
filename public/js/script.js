console.log('script');
let socket = io('http://105.78.234.75:3000');

/* button status from client */
socket.on('light', function (data) {
  
  /* change checkbox according to push button on Raspberry Pi */
  document.getElementById('light').checked = data;

  /* send push button status to back to server */
  socket.emit('light', data);
});

window.addEventListener('load', function() {
  let lightbox = document.getElementById('light');

  /* event listener when checkbox changes */
  lightbox.addEventListener('change', function() {

     /* send button status to server (as 1 or 0) */
    socket.emit('light', Number(this.checked));
  });
});