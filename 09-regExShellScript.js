console.log('regExShellScript.js');
/* dependencies */
	const exec = require('child_process').exec;

/* scripts */
	const pathToScript = '/home/pi/Desktop/projects/nodetest/scripts/script_getInfos';

/* get pis infos */
exec(pathToScript, function(error, stdout, stderr) {
	if (error != null) console.log('exec error: ', error);
	if (stderr) console.log('stderr: ', stderr);

	console.log('stdout:', stdout);

	/* finding Uptime */
		let findUptime = /up[a-z0-9,\s]*minutes|hours|day/gi;

		let currentUptime = stdout.match(findUptime);
		if (currentUptime) {
			console.log('Uptime: ', currentUptime[0]);
		} else {
			console.log('Uptime: Currentlly this info is not available!');
		}

	/* test */
		let text = '<h1>Winter is coming</h1>';

		let tagRegex = /<h1>|<\/h1>/gi;
		let findTag = text.match(tagRegex);
		if (findTag) {
			console.log('find Tag: ', findTag[0]);

		} else {
			console.log('There has not been a match!');
		}

		let tagAndValRegex = /<h1>[a-z\s]*<\/h1>/gi;
		let findTagAndVal = text.match(tagAndValRegex);
		if (findTagAndVal){
			console.log('find Tag and Value: ', findTagAndVal[0]);

		} else {
			console.log('There has not been a match!');
		}

		let valRegex = />[a-z\s]*</gi;
		let findVal = text.match(valRegex);
		console.log('find Value: ', findVal[0]);

	/* finding Temp */
		let findCPUTemp = /[0-9][0-9]\.[0-9]'c/gi;

		let currentTemp = stdout.match(findCPUTemp);
		/* watch out currentTemp is an object
		console.log('typeof: ', typeof currentTemp); */
		if (currentTemp) {
			console.log('Temp: ', currentTemp[0]);

		} else {
			console.log('There has not been a match!');
		}
});