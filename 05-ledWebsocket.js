console.log('ledWebSockets.js');

/* dependencies */
var app = require('http').createServer(handler);
var fs = require('fs');
var io = require('socket.io')(app);
var Gpio = require('onoff').Gpio;

var LED = new Gpio(17, 'out');
var pushButton = new Gpio(27, 'in', 'both'); //use GPIO pin 17 as input, and 'both' button presses, and releases should be handled

const port = 3000;

function handler (req, res) {
	fs.readFile(__dirname + '/public/index.html', function(err, data) { //read file index.html in public folder
		if (err) {
			res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
			return res.end("404 Not Found");
		}
		res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
		res.write(data); //write data from index.html
		return res.end();
	});
}

/* web socket connection */
io.sockets.on('connection', function (socket) {

	/* set status variable of led to 0 */
	var lightvalue = 0;

	/* listener on pushButton */
	pushButton.watch(function (err, value) {
		if (err) {
			return console.error('There was an error', err);
		}

		/* if button pushed change status variable to value of button */
		lightvalue = value;

		/* send button status to client */
		socket.emit('light', lightvalue);
 	});
	
	/* get light switch status from client */
	socket.on('light', function(data) {
		lightvalue = data;

		/* only change LED if status has changed */
		if (lightvalue != LED.readSync()) {

			/* turn LED on or off */
			LED.writeSync(lightvalue);
		}
	});
});

/* by pressing CTRL + C it cleans everything up */
process.on('SIGINT', function () {
	LED.writeSync(0);
	LED.unexport();
	pushButton.unexport();
	process.exit();
});

/* server listening on port */
app.listen(port, () => {
  console.log(`-- App: Started up at port ${port}`);
});