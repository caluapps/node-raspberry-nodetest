/* ledWebsocket */
console.log('ledWebsocket.js');

/* requirements */
// const path = require('path');
const http = require('http');
// const fs = require('fs'); //require filesystem module
const express = require('express');
const socketIO = require('socket.io');
const hbs = require('hbs');

/* pins */
const Gpio = require('onoff').Gpio;
const LED = new Gpio(17, 'out');
const pushButton = new Gpio(27, 'in', 'both');

/* clear path
const publicPath = path.join(__dirname, '../public'); */

/* Port */
const port = process.env.PORT || 3000;

/* app, server, io */
let app = express();
let server = http.createServer(app);
let io = socketIO(server);

/* ledWebsocket LED 17
LED.writeSync(1); */

app.set('view engine', 'hbs');

/* create server */
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
	if (err) {
		return console.log('Error: ', err);
	}
	res.send('/public/index.html');
});

/* WebSocket connection */
io.on('connection', (socket) => {
	console.log('connect');

	// static variable for current status
	var lightvalue = 0;

	/* Watch for hardware interrupts on pushButton */
	pushButton.watch((err, value) => {
		// Error handler
		if (err) {
			console.error('There was an error', err);
			return;
		}
		// event
		console.log('server changed: ', value);
		lightvalue = value;
		socket.emit('light', lightvalue);
	});

	/* get light switch status from client */
	socket.on('light', (data) => {
		console.log('socket.on: light');
		console.log('data: ', data);
		lightvalue = data;
		// only change LED if status has changed
		
		if (lightvalue != LED.readSync()) {
			// turn LED on or off
			LED.writeSync(lightvalue);
		}
		LED.writeSync(1);
	});

	socket.on('disconnect', () => {
		console.log('disconnected from server');
	});
});

/* by pressing CTRL + C it cleans everything up */
process.on('SIGINT', function () {
	LED.writeSync(0);
	LED.unexport();
	pushButton.unexport();
	process.exit();
}); 

/* server listening on port */
app.listen(port, () => {
  console.log(`-- App: Started up at port ${port}`);
});