/* buttonLed */
console.log('buttonLed.js');
/* requirements */
const Gpio = require('onoff').Gpio;

/* pins */
const ledPin = new Gpio(17, 'out');
const pushButton = new Gpio(27, 'in', 'both');
/* debouncing Button
const pushButton = new Gpio(4, 'in', 'rising', {debounceTimeout: 10}); */

/* function to change value of pins */
const useLed = (led, value) => led.writeSync(value);
/* function to clean everything up */
const unexportOnClose = () => {
	ledPin.writeSync(0);
	ledPin.unexport();
	pushButton.unwatchAll();
	pushButton.unexport();
	console.log('end of program');
}

/* listener on pin w/ button */
pushButton.watch((err, value) => {
	console.log('value: ', value);
	if (err) {
		console.error('There was an error', err);
		throw err;
	}
	useLed(ledPin, value);

	/* debouncing button
	led.writeSync(led.readSync() ^ 1); */
});

/* by pressing CTRL + C it cleans everything up */
process.on('SIGINT', unexportOnClose);